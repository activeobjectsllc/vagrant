#!/bin/bash
#========================     REINSTALL PERSONAL KEYS    ==================================#

sudo cp /vagrant/ssh_keys/id_rsa* ~/.ssh/
sudo chown vagrant:vagrant ~/.ssh/id_rsa*
sudo chmod 600 ~/.ssh/id_rsa* && eval `ssh-agent -s`
sudo ssh-add ~/.ssh/id_rsa