# -*- mode: ruby -*-
# vi: set ft=ruby :
# Documentation https://docs.vagrantup.com.

# If you have problems mounting the shared folder try
# vagrant plugin install vagrant-nfs_guest

box         = 'activeobjects/vagrantao'
boxver      = '0.1.1'
hostname    = 'vagrantao'
domain      = 'activeobjects.net'
privip      = '192.168.0.6'
ram         = '4096'
cores       = '4'
sharetype   = 'nfs'

Vagrant.configure(2) do |config|  
  config.vm.box = box
  config.vm.boot_timeout = 300    
  config.vm.host_name = hostname + '.' + domain
  
  #config.vm.box_check_update = false

  # Foward your host machine ports: 8080 on parent
  config.vm.network "forwarded_port", guest: 80, host: 8080

  # SSH config to avoid key problems
  #config.ssh.insert_key = false
  config.ssh.username = "vagrant"
  config.ssh.password = "vagrant"

  # Foward your host machine ports
  #config.vm.network "forwarded_port", guest: 80, host: 80
  # if the above doesnt work try this
  #config.vm.network "forwarded_port", guest: 80, host: 8080
  
  config.vm.network "private_network", ip: privip
  #config.vm.network "public_network"
  
  #config.vm.synced_folder "../data", "/vagrant_data"


  # This line for Windows
  #config.vm.synced_folder ".", "/vagrant", type: sharetype

  # This line for Mac
  config.vm.synced_folder ".", "/vagrant", type: sharetype

  # Share Folders for Windows Parent
  #config.vm.synced_folder "www", "/var/www", id: "vagrant-www", type: sharetype, group: 'www-data', owner: 'vagrant', mount_options: ["dmode=777", "fmode=764"]  
  
  # Share Folders these lines for Mac Parent
  #config.vm.synced_folder "www", "/var/www", id: "vagrant-www", type: sharetype, mount_options: ["dmode=777", "fmode=764"]  
  
  config.vm.synced_folder "sql", "/home/vagrant/Downloads/sql", type: sharetype, id: "db-dumps"
  
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
    vb.memory = ram            # Customize the amount of memory on the VM:
    vb.cpus = cores
    vb.customize ["modifyvm", :id, "--name", hostname]
    # Bi-Directional clipboard, USB, hostname and IP configs
    vb.customize ["modifyvm", :id, "--name", hostname, "--clipboard", "bidirectional", "--usbehci", "off"]

    # If you are having network problems you may want to uncomment the following on windows parents
    #vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    #vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
  end
  
  # Atlas Configuration
  # Comment this line if you are not using atlas
  config.vm.box_version = boxver  

  #config.push.define "atlas" do |push|
  #   push.app = box
  #end

  # Fix the problem with insecure private key
  # Remove windows line endings from files
  config.vm.provision "shell", inline: <<-SHELL             
    sudo apt-get install -y dos2unix
    dos2unix /vagrant/vagrantprovision/vagrantprovision.sh
    dos2unix /vagrant/vagrantprovision/vagrantprovision_optional.sh
  SHELL
  
  config.vm.provision "shell", inline: "/bin/bash /vagrant/vagrantprovision/vagrantprovision.sh", privileged: true
end
