# == Class: baseconfig
#
# Performs initial configuration tasks for all Vagrant boxes.
#
class activeobjects {

  file {
    '/mnt/tmp':
      owner => vagrant,
      group => vagrant,
      ensure  => link,
      mode => '777',
      target  => "/tmp";

    '/home/vagrant/.ssh':
      owner => vagrant,
      group => vagrant,
      ensure => directory;

    '/home/vagrant/.ssh/id_rsa':
      source  => '/vagrant/ssh_keys/id_rsa',
  	  owner => vagrant,
  	  group => vagrant,
      mode => '600';

    '/home/vagrant/.ssh/id_rsa.pub':
      source  => '/vagrant/ssh_keys/id_rsa.pub',
  	  owner => vagrant,
  	  group => vagrant,
      mode => '600';
  }

  exec {"load-latest-provisioner":
      command =>"/bin/bash /vagrant/modules/scripts/batch_script.sh",
      require => Package["git"],
  }
}
