#!/bin/bash
#========================     PROVISION OPTIONAL COMPONENTS BOX    ==================================#

# SSH Keys
sudo cp /vagrant/ssh_keys/id_rsa* ~/.ssh/ && sudo chown vagrant:vagrant ~/.ssh/id_rsa* && sudo chmod 600 ~/.ssh/id_rsa* && eval `ssh-agent -s` && sudo ssh-add ~/.ssh/id_rsa
		
# Site-Php folder
sudo mkdir -p /var/www/site-php && sudo chown vagrant:vagrant /var/www/site-php && cd /var/www/site-php && git clone git@bitbucket.org:activeobjectsllc/provisioner.git . && git pull

#configs and aliases
#/var/www/site-php/scripts/installers/ssh_config.sh && /var/www/site-php/scripts/installers/bash_aliases.sh && source ~/.bash_aliases

#Need A desktop
#sudo apt-get install lubuntu-desktop -y
#sudo apt-get remove light-locker -y
#sudo apt-get install -y xscreensaver

#Need flash and java in browser?
#sudo apt-get install ubuntu-restricted-extras

#Upgrade Kernel from http://www.circuidipity.com/lubuntu-trusty-hacks.html
#wget -c http://kernel.ubuntu.com/~kernel-ppa/mainline/v3.17.1-utopic/linux-headers-3.17.1-031701-generic_3.17.1-031701.201410150735_amd64.deb
#wget -c http://kernel.ubuntu.com/~kernel-ppa/mainline/v3.17.1-utopic/linux-headers-3.17.1-031701_3.17.1-031701.201410150735_all.deb
#wget -c http://kernel.ubuntu.com/~kernel-ppa/mainline/v3.17.1-utopic/linux-image-3.17.1-031701-generic_3.17.1-031701.201410150735_amd64.deb
#sudo dpkg -i linux*.deb

#Need a terminal
#sudo apt-get install -y terminator

#Java
#/var/www/site-php/scripts/installers/java.sh

#Composer
#/var/www/site-php/scripts/installers/composer.sh
		
#Apache SSL
#/var/www/site-php/scripts/installers/apache_ssl.sh

#Postgres
#sudo apt-get install -y php5-pgsql

#PHP Unit
#/var/www/site-php/scripts/installers/phpunit.sh

#Sublime Text
#/var/www/site-php/scripts/installers/sublimetext.sh

#PHP Storm
#/var/www/site-php/scripts/installers/phpstorm.sh

#Wine
#/var/www/site-php/scripts/installers/wine.sh

#Chrome
#/var/www/site-php/scripts/installers/chrome.sh

#Smart Git
#/var/www/site-php/scripts/installers/smartgit.sh

#pico /home/vagrant/.config/openbox/lubuntu-rc.xml
#add the following under keybindings section
#	<!-- Custom Keybindings -->
#    <keybind key="C-t">
#      <action name="execute">
#        <command>terminator</command>
#      </action>
#    </keybind>
#    <keybind key="C-m">
#      <action name="execute">
#        <command>dmenu-run.sh</command>
#      </action>
#    </keybind>

#Collapsible Menu
#sudo apt-get install suckless-tools
# place the following in ~/.bin/dmenu-run.sh
# #!/bin/bash
# dmenu_run -b -i -fn '10x20' -nb '#000000' -nf '#ffffff' -sb '#d64937' -sf '#000000'
