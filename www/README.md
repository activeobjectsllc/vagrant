# Local WWW folder  #

This is where all of your local files will be located including the provisioner and each cloned repository

## Tips ##

To get files showing locally, use the following piece of code and place it in your _.htaccess_ file. Replace the url in the last line with the site you're working on. For the RSNs you can use any of the RSN domains.

```
  # Force image styles that have local files that exist to be generated.
  RewriteCond %{REQUEST_URI} ^/sites/([^\/]*)/files/styles/[^\/]*/public/((.*))$
  RewriteCond %{DOCUMENT_ROOT}/sites/%1/files/%2 -f
  RewriteRule ^(.*)$ $1 [QSA,L]
  # Otherwise, send anything else that's in the files directory to the
  # production server.
  RewriteCond %{REQUEST_URI} ^/sites/[^\/]*/files/.*$
  RewriteCond %{REQUEST_URI} !^/sites/[^\/]*/files/css/.*$
  RewriteCond %{REQUEST_URI} !^/sites/[^\/]*/files/js/.*$
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule ^(.*)$ http://www.nbcsports.com/$1 [QSA,L]
```

If this doesn't work for all of your images, try to remove the first 4 lines.

```
// For NBCSports.com
  # Otherwise, send anything else that's in the files directory to the
  # production server.
  RewriteCond %{REQUEST_URI} ^/files/.*$
  RewriteCond %{REQUEST_URI} !^/files/css/.*$
  RewriteCond %{REQUEST_URI} !^/files/js/.*$
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule ^(.*)$ http://www.nbcsports.com/$1 [QSA,L]


// For the RSNs (for Bay Area, use csnbayarea.com)
  # Otherwise, send anything else that's in the files directory to the
  # production server.
  RewriteCond %{REQUEST_URI} ^/sites/[^\/]*/files/.*$
  RewriteCond %{REQUEST_URI} !^/sites/[^\/]*/files/css/.*$
  RewriteCond %{REQUEST_URI} !^/sites/[^\/]*/files/js/.*$
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule ^(.*)$ http://www.csnne.com/$1 [QSA,L]
```