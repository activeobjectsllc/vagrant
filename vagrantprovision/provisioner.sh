#!/bin/bash
#========================     SET UP THE PROVISIONER    ==================================#

#Copy and setup ssh keys
eval `ssh-agent -s`
cp /vagrant/ssh_keys/id_rsa* ~/.ssh/
chmod 600 ~/.ssh/id_rsa*
ssh-add ~/.ssh/id_rsa*

# Install the provisioner
sudo chown vagrant:www-data /var/www
git clone git@bitbucket.org:activeobjectsllc/provisioner.git /var/www/site-php

# Install SSH Config and Aliases
/var/www/site-php/scripts/installers/ssh_config
/var/www/site-php/scripts/installers/bash_aliases

echo ''
echo 'Now Run:'
echo 'source ~/.bash_aliases'