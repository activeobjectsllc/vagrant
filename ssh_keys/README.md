# Setup SSH Keys on GitHub  #

Prerequisite: Copy your ssh key files to this folder ie. id_rsa and id_rsa.pub. The VM will create symbolic links to these files which will enable you to run git from the command line without having to enter your password each time.

For additional help on setting up SSH Keys, visit https://help.github.com/articles/generating-ssh-keys

The config file in this folder contains info on common ssh configs that the team uses feel free to add
