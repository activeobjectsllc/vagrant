# == Class: apache
#
# Installs packages for Apache2, enables modules, and sets config files.
#
class apache {
  package { ['apache2', 'apache2-mpm-prefork']:
    ensure => present;
  }

  service { 'apache2':
    ensure  => running,
    require => Package['apache2'];
  }
  
  file {
    '/etc/apache2/sites-available':
      owner => vagrant,
      group => vagrant,
      ensure  => directory,
      mode => '777';

    '/etc/apache2/sites-enabled':
      owner => vagrant,
      group => vagrant,
      ensure  => directory,
      mode => '777';
    
  }
}