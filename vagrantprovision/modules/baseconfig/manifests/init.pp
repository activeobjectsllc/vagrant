# == Class: baseconfig
#
# Performs initial configuration tasks for all Vagrant boxes.
#
class baseconfig {
  exec { 'apt-get update':
    command => '/usr/bin/apt-get update'
  }

  #host { 'hostmachine':
  #  ip => '192.168.33.10';
  #}

  # Make sure site-php directory is present
  file { 
    '/etc/puppet/files':
      ensure => directory;
  }
}
