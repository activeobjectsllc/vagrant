#!/bin/bash
#========================     PROVISION VAGRANT BOX    ==================================#
sudo apt-get update
sudo aptitude update
sudo aptitude -y upgrade
		
#Update Hostname
sudo sed -i 's/vagrant-ubuntu-trusty-64/loc/g' /etc/hostname	

#Basic requirements
sudo apt-get install -y dos2unix xclip
sudo apt-get install -y git curl rsync imagemagick xrdp cifs-utils dnsmasq

#Drupal
sudo apt-get install -y drush

#Mysql		
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password '
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password '
sudo apt-get -y install mysql-server

#LAMP Apache PHP
sudo apt-get install -y apache2 mysql-client-5.5 php5 php5-cli php-apc php5-curl php5-ldap php5-xmlrpc libapache2-mod-php5 php5-xdebug php5-mysql php5-mcrypt
sudo chmod 777 -Rf /etc/apache2/sites-available && sudo chmod 777 -Rf /etc/apache2/sites-enabled

#Memcache And Varnish
sudo apt-get install -y memcached php5-memcached varnish
		
#apache specific modules
sudo a2enmod proxy proxy_http headers rewrite
sudo service apache2 restart		

#Copy Configuration files
sudo cp /vagrant/vagrantprovision/ports.conf 	/etc/apache2/ports.conf
sudo cp /vagrant/vagrantprovision/xdebug.ini 	/etc/php5/mods-available/xdebug.ini
sudo cp /vagrant/vagrantprovision/apcu.ini 		/etc/php5/mods-available/apcu.ini	

#Fix DNSMasq
sudo sed 's/\#conf-dir\=\/etc\/dnsmasq.d/conf-dir\=\/etc\/dnsmasq.d/g' /etc/dnsmasq.conf
sudo mkdir -p /etc/dnsmasq.d
sudo cp /vagrant/vagrantprovision/domains.conf /etc/dnsmasq.d && sudo service dsnmasq restart

#PHP short open tags
#sudo cp /vagrant/vagrantprovision/php.ini 		/etc/php5/cli/php.ini
sudo sed -i 's/short_open_tag = Off/short_open_tag = On/g' /etc/php5/cli/php.ini 
sudo sed -i 's/short_open_tag = Off/short_open_tag = On/g' /etc/php5/apache2/php.ini
sudo service apache2 restart

#Add more Inodes
sudo sed 's/\# Log Martian Packets/fs.inotify.max_user_watches = 524288/g' /etc/sysctl.conf
sudo sysctl -p

#Change SSH mode to non strict
sudo sed -i 's/StrictModes yes/StrictModes no/g' /etc/ssh/sshd_config && sudo service ssh restart

#Directory Permissions
sudo chown vagrant:vagrant /home/vagrant/Downloads
sudo chown vagrant:www-data /var/www

