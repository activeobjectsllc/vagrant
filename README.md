# Development Setup for Active Objects LLC Websites #
## Prerequisites ##

1. Vagrant
2. Virtualbox

## Overview ##
LAMP stack setup for PHP developers. This setup of this VM to allow all development to be done locally in an environment that closely matches production servers and other developers.

## Details ##
This vagrant build will take care of the basic LAMP Stack tools needed to run AO websites. There is no need to install PHP, MySQL or Apache etc to your machine locally as they have been automatically loaded into the box.

---

Follow the instructions below to setup a basic LAMP Development environment for the AO sites. You must be running the latest version of Vagrant and Virtualbox as some older versions are not compatible.

Download and install:

1. Virtualbox - www.virtualbox.org

1. Vagrant - www.vagrantup.com

1. Install ssh security plugin `vagrant plugin install vagrant-rekey-ssh`


#Import the box
1. Clone this repo `git clone https://bitbucket.org/activeobjectsllc/vagrant.git`
If you are using a windows parent you may need to set up a [git windows client](https://windows.github.com/) or if you have git already run the following command in your 'My Documents' folder
This will give you the Vagrant configuration which you need to get started

##External Developers
1. Download the box from the link which you will receive in email

1. In the same folder where you downloaded the file run the following commands
```bash
vagrant box add activeobjects/vagrantao vagrantao.box
```

##Internal Developers
1. Sign up at atlas.hashicorp.com and send your username to swalker@activeobjects.net

1. Loging to Atlas `vagrant login` and Enter Atlas User Name and password


1. Change these lines in your `Vagrantfile`
```bash
#Uncomment the following line
#config.vm.box_version = boxver
```

##Prefer a GUI?
Before you start edit the [Vagrantfile](src/master/Vagrantfile) in order to turn on GUI mode (and maybe tweak memory settings, IP addresses etc )

```bash
config.vm.provider "virtualbox" do |v|
  #Uncomment the following line to turn on gui mode if you need it
  # v.gui = true        # Uncomment to enable a gui
  
  #If you are using a mac parent then uncomment the lines below
  #Share Folders these lines for Mac Parent
  #config.vm.synced_folder "www", "/var/www", id: "vagrant-www", type: sharetype, mount_options: ["dmode=777", "fmode=764"]  
  #config.vm.synced_folder "www", "/var/www", id: "vagrant-www", type: sharetype, mount_options: ["dmode=777", "fmode=764"]  
end
```

# Start the box
```bash
vagrant up
``` 
##Password
If you are prompted for a password when the box is starting use the password below which is also the sudo password

User/Pass: __vagrant__  / __vagrant__


# SSH Keys Setup
The ssh key provides security for code repositories and servers on the AO network. Set up new ssh key as follows:

#If you do not have keys then run this commands in the box:
```bash
ssh-keygen -t rsa -C'youremail@activeobjects.net' && eval `ssh-agent -s` && ssh-add id_rsa

#copy the id_rsa.pub to your shared folder for safe keeping incase your box stops working
cp ~/.ssh/id_rsa* /vagrant/ssh_keys
#you can then get it from your host machine in the vagrant folder
#Send the public key (~/.ssh/id_rsa.pub) to swalker@activeobjects.net
```

#If you have existing keys make sure they are in /vagrant/ssh_keys and then run this command in the box instead:
```bash
/vagrant/vagrantprovision/copykeys.sh
```



##Port Forwarding
If using the box headless then remember to uncomment
`#config.vm.network "forwarded_port", guest: 80, host: 80`


##Sample Host Entries
If you are using the box headless you will need to create host entries on your parent machine
These etnries go into `/etc/hosts` on unix parent or in `c:\windows\system32\drivers\etc\hosts` on Windows Parent
```bash
127.0.0.1       www.sitename.loc	sitename.loc
127.0.0.1       cdn.sitname.loc		subdomain.sitename.loc
```

#Setup Provisioner
Full Instructions are [here](https://bitbucket.org/activeobjectsllc/provisioner)
```bash
#Run this inside the box

#Clone Provisioner
/vagrant/vagrantprovision/provisioner.sh

#Run the provisioner
provision
```

---

#Notes
1. you can run `vagrant ssh` on your parent machine to connect your box via ssh